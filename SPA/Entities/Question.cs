﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SPA.Entities
{
    public class Question
    {
        [Key]       
        public int QuestionId { get; set; }
        public string Title { get; set; }

        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }
        public AnswerOption CorrectAnswer { get; set; }

        public virtual ICollection<Answer> Answers { get; set; }
    }
}