﻿

angular.module("myModule", [])
.controller("quizController", function ($scope, $http) {

    $scope.answered = false;
    $scope.title = "loading question...";
    $scope.options = [];
    $scope.correctAnswer = false;
    $scope.working = false;

    $scope.answer = function () {
        return $scope.correctAnswer ? 'correct' : 'incorrect';
    };

    $scope.nextQuestion = function() {
            $scope.working = true;
            $scope.answered = false;
            $scope.title = "loading question...";
            $scope.options = [];


        $http.get("/api/Quiz").success(function (data) {

            $scope.options = [
                {
                    name: data.answerA,
                    chosen: "A"
                },
                {
                    name: data.answerB,
                    chosen: "B"
                },
                {
                    name: data.answerC,
                    chosen: "C"
                },
                {
                    name: data.answerD,
                    chosen: "D"
                }
            ];                              
                $scope.questionId = data.questionId; 
                $scope.title = data.title;
                $scope.answered = false;
                $scope.working = false;
            }).error(function(data, status, headers, config) {
                $scope.title = "Oops... something went wrong";
                $scope.working = false;
            });
    }


    $scope.sendAnswer = function (questionId, answer) {
        $scope.working = true;
        $scope.answered = true;

        $http.post("/api/Quiz", { 'questionId': questionId, 'chosenAnswer': answer}).success(function (data) {
            $scope.correctAnswer = (data === true);
            $scope.working = false;
            $scope.nextQuestion();
        }).error(function () {
            $scope.title = "Oops... something went wrong";
            $scope.working = false;
        });
    };



    });



