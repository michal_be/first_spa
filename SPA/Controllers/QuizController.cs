﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SPA.Entities;
using SPA.Models;

namespace SPA.Controllers
{
    public class QuizController : ApiController
    {
        private ApplicationDbContext context = new ApplicationDbContext();
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                context.Dispose();
            }

            base.Dispose(disposing);
        }


        // GET api/Quiz
        [ResponseType(typeof(QuestionViewModel))]
        public async Task<IHttpActionResult> Get()
        {
            Random rand = new Random();
            int toSkip = rand.Next(2, context.Questions.Count() + 2);

            Question nextQuestion = await context.Questions.FirstOrDefaultAsync(e => e.QuestionId == toSkip);

            var viewModel = new QuestionViewModel()
            {
                QuestionId = nextQuestion.QuestionId,
                Title = nextQuestion.Title,
                AnswerA = nextQuestion.AnswerA,
                AnswerB = nextQuestion.AnswerB,
                AnswerC = nextQuestion.AnswerC,
                AnswerD = nextQuestion.AnswerD,
                CorrectAnswer = nextQuestion.CorrectAnswer,
            };

            return this.Ok(viewModel);
        }

        // POST api/Quiz
        [ResponseType(typeof (AnswerViewModel))]
        public async Task<IHttpActionResult> Post(AnswerViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return this.BadRequest(this.ModelState);
            }

            Answer answer = new Answer()
            {
                ChosenAnswer = viewModel.ChosenAnswer,
                QuestionId = viewModel.QuestionId
            };

            context.Answers.Add(answer);
            await context.SaveChangesAsync();

            var correctAnswer = await context.Questions.FirstOrDefaultAsync(x => x.QuestionId == answer.QuestionId);
            bool isCorrect = (answer.ChosenAnswer == correctAnswer.CorrectAnswer);

            return Ok<bool>(isCorrect);

        }

        

    }
}
