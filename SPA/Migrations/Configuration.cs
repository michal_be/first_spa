using System.Collections.Generic;
using SPA.Entities;

namespace SPA.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<SPA.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(SPA.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            if (!context.Questions.Any())
            {

                List<Question> list = new List<Question>()
                {
                    new Question()
                    {
                        Title = "W kt�rym roku ukaza�o si� pierwsze wydanie Framweorka AngularJS ?",
                        AnswerA = "1990",
                        AnswerB = "2000",
                        AnswerC = "2009",
                        AnswerD = "2011",
                        CorrectAnswer = AnswerOption.C
                    },
                    new Question()
                    {
                        Title = "Jakim typem j�zyka programowania jest C# ?",
                        AnswerA = "Obiektowym",
                        AnswerB = "Strukturalnym",
                        AnswerC = "Wieloparadygmatowym",
                        AnswerD = "�adnym z wymienionych",
                        CorrectAnswer = AnswerOption.A
                    },
                    new Question()
                    {
                        Title = "Kto jest tw�rc� j�zyka C# ?",
                        AnswerA = "Bjarne Stroustrup",
                        AnswerB = "Anders Hejlsberg",
                        AnswerC = "James Gosling",
                        AnswerD = "Linus Torvalds",
                        CorrectAnswer = AnswerOption.B
                    },
                    new Question()
                    {
                        Title = "Rozwi� skr�t MVC",
                        AnswerA = "Model Valid Class",
                        AnswerB = "Many Variable Class",
                        AnswerC = "Model Viariable Context",
                        AnswerD = "Model View Controller",
                        CorrectAnswer = AnswerOption.D
                    },

                };

                foreach(var question in list) {
                    context.Questions.Add(question);
                    context.SaveChanges();
                }
             
            }
            
        }
    }
}
