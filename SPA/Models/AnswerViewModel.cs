﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SPA.Entities;

namespace SPA.Models
{
    public class AnswerViewModel
    {
        public int AnswerId { get; set; }
        public int QuestionId { get; set; }
        public AnswerOption ChosenAnswer { get; set; }

    }
}