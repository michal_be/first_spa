﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SPA.Entities;

namespace SPA.Models
{
    public class QuestionViewModel
    {

        public int QuestionId { get; set; }
        public string Title { get; set; }

        public string AnswerA { get; set; }
        public string AnswerB { get; set; }
        public string AnswerC { get; set; }
        public string AnswerD { get; set; }
        public AnswerOption CorrectAnswer { get; set; }


    }
}