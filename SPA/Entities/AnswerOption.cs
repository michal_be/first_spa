﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SPA.Entities
{
    public enum AnswerOption
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
    }
}